# Pipes and Paper 2.0 - reMarkable Whiteboard

This is a tool for imitating a Whiteboard for presentations. The idea and a big chunk of the code comes from Joe Wass: https://gitlab.com/afandian/pipes-and-paper/. He has also more explanation on how this works from a technical point of view.

It features support for the eraser on the Marker Plus and the Button on the LAMY AL-star black EMR, a highlighter, a laser pointer and the ability to save your write-up.

Feel free to try it out. If you discover bugs or have suggestions for additional features, please let me know!

## What is this good for?

I wanted to have something like this for online talks and teaching. With the pandemic, online teaching was mostly done by sharing the screen of a tablet. I never really liked this for the following reasons:

1. The limited space. If you give a blackboard lecture/talk, one of the greatest advantages is to have the write-up available for a long time. As a listener you can still see the definition from 10 minutes ago, in case you forgot. With shared tablet screen, the content is gone after a few minutes, maybe even less. 
2. The controls. It is kind of distracting (at least for me) to see the user interface and all the on-screen controls. Also they again take up space that is needed for your notes.

When I got my reMarkable I was hoping for the in-built live-view feature. This was rather disappointing because of lag and reliability. After a quick search, I found Joe Wass' Pipes and Paper and realized, this was just what I was looking for. 

While I know close to nothing about Python, I can do a bit of coding in JavaScript and here's the result. It is supposed to imitate a seminar or a class room with chalkboards.

## In Action

Here's a short video demonstration:

[Demo](additional/remarkable_whiteboard.mp4)


## How to use

The following instruction is copied from https://gitlab.com/afandian/pipes-and-paper/. 

You'll need to do a little bit on the command-line. Assuming you have Python 3 installed.

Setup:

1. Set up SSH private keys and config so that running `ssh remarkable_usb` succeeds. This works both via wifi and USB connection.
    -  If you want to connect via WiFi you will need to retrieve the IP address from the help screen.
    -  If you want to use it with the wired connection, use the IP address `10.11.99.1`.
2. Install the requirements for the python script that runs on your computer:
    - `pip3 install -r requirements.txt`

To run:

1. `./run_usb`
2. Visit <http://localhost:6789/>

For an explanation on how it works, see https://gitlab.com/afandian/pipes-and-paper/.

## Keyboard inputs

- 'Space' for download. There is an automatic download every time a new canvas is opened.
- 'enter' for new canvas.
- 'r' or '*' (number pad) for rotate.
    - Only the last (most right) canvas can be rotated. This is to avoid overlap when a canvas in the middle is rotated. If you add a new canvas, orientation will be the same as the last (most right) one.
- 'h' or '0' (number pad) for turning the highlighter mode on. Stays on until key is released. 
- 'p' or '.' (number pad) for turning the laser pointer mode on. Stays on until key is released. 
- '1' - '9' for switching line color to next setting (costumizable by changing var LineColors). Default setting:

          1   :   black
          2   :   blue
          3   :   gray
          4   :   green
          5   :   red 
          6   :   orange
          7   :   violet
          8   :   yellow
          9   :   light blue
             
-  '<' and '>' (or '+' and '-' on number pad) to select a different canvas.
- 'q' for enabling delete canvas (to avoid accidental deleting).
- 'd' for deleting context of current canvas. Needs to be enabled with 'q' first.
- 'c' for checking which mode you are in (highlighter, laserpointer or normal writing): 
    - there will appear a small square in the upper right corner of the current canvas which is deleted on releasing the key

You can use an (external) number pad to use these features. In the folder 'additional' there is a sheet of symbols one can use. Here's my example:

![External number pad](additional/numpad.png)

## Inserting images

By clicking on your selected canvas you can insert an image onto the canvas. If you are too close to the right edge, the image is scaled. 

